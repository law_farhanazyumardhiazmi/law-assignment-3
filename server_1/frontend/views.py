import uuid

import requests
from django.conf import settings
from django.shortcuts import render

from frontend.forms import UploadFileForm


def process_file(request, in_memory_file):
    url = f"{settings.SERVER_2_URL}/receive-file/"
    routing_key = str(uuid.uuid4())
    headers = {
        "X-ROUTING-KEY": routing_key,
    }
    files = {
        "file": (in_memory_file.name, in_memory_file.file, in_memory_file.content_type)
    }
    response = requests.post(url=url, headers=headers, files=files)
    return render(
        request,
        "progress.html",
        {
            "response": response.json()["response"],
            "file_name": in_memory_file.name,
            "routing_key": routing_key,
        },
    )


def upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            return process_file(request, request.FILES["file"])
        else:
            form = UploadFileForm(request.POST, request.FILES)
    else:
        form = UploadFileForm()
    return render(request, "upload.html", {"form": form})
