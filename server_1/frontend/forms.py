from django.forms import forms, widgets


class UploadFileForm(forms.Form):
    file = forms.FileField(widget=widgets.FileInput(attrs={"class": "form-control"}))
