# Layanan dan Aplikasi Web - Tugas 3

## Farhan Azyumardhi Azmi - 1706979234 - Kelas A

# Instalasi dan Cara Menjalankan Aplikasi

Requirements: Docker dan Docker Compose

Untuk menjalankan aplikasi, jalankan perintah:

```docker-compose up -d --build```

Saat pertama kali menjalankan aplikasi, jalankan perintah migrate untuk aplikasi server_1 dan server_2:

```docker-compose exec server_1 python manage.py migrate```

```docker-compose exec server_2 python manage.py migrate```
