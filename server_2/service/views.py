import requests
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# TODO enable CSRF
from service.models import UploadedFile
from service.tasks import compress_file


@csrf_exempt
def receive_file(request):
    if request.method == "POST":
        try:
            file = request.FILES["file"]
            uploaded_file_object = UploadedFile.objects.create(file=file)
            compress_file.delay(
                uploaded_file_object.id, request.headers["X-ROUTING-KEY"]
            )
            return JsonResponse(
                data={"response": "File uploaded successfully"}, status=200
            )
        except requests.exceptions.RequestException:
            return JsonResponse(data={"error": "Something wrong happened"}, status=500)
    else:
        return JsonResponse(data={"error": "Method not allowed"}, status=405)
