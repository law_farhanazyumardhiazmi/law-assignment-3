from django.contrib import admin

from service.models import UploadedFile

admin.site.register(UploadedFile)
