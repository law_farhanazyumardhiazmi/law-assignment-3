from __future__ import absolute_import, unicode_literals

import gzip
import io
import math
import os

import pika
from celery import shared_task
from django.conf import settings
from django.core.files.storage import default_storage

from service.models import UploadedFile


@shared_task
def compress_file(uploaded_file_id, routing_key):
    uploaded_file_object = UploadedFile.objects.get(id=uploaded_file_id)
    file = uploaded_file_object.file
    cumulative_percentage = cumulative_processed_size = 0
    compressed_file_bytes = io.BytesIO()
    with gzip.GzipFile(
        filename=file.name + ".gz", mode="wb", fileobj=compressed_file_bytes,
    ) as gzip_writer:
        file_chunks = file.chunks(chunk_size=file.size // 10)
        for chunk in file_chunks:
            gzip_writer.write(chunk)
            cumulative_processed_size += len(chunk)
            percentage = math.ceil(100 * cumulative_processed_size / file.size)
            if (
                percentage > 0
                and percentage % 10 == 0
                and percentage != cumulative_percentage
            ):
                publish_progress.delay(percentage, routing_key)
                cumulative_percentage = percentage

    cleanup_file.delay(uploaded_file_id)


@shared_task
def publish_progress(percentage, routing_key):
    exchange_name = "1706979234"
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST,
            port=settings.RABBITMQ_PORT,
            virtual_host=settings.RABBITMQ_VHOST,
            credentials=pika.PlainCredentials(
                username=settings.RABBITMQ_USERNAME, password=settings.RABBITMQ_PASSWORD
            ),
        )
    )
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange_name, exchange_type="direct")
    channel.basic_publish(
        exchange=exchange_name,
        routing_key=routing_key,
        body=bytes(str(percentage), encoding="UTF-8"),
        mandatory=True,
    )


@shared_task
def cleanup_file(uploaded_file_id):
    uploaded_file_object = UploadedFile.objects.get(id=uploaded_file_id)
    file = uploaded_file_object.file
    if hasattr(file, "path") and os.path.exists(file.path):
        try:
            default_storage.delete(file.path)
        except:
            pass
    uploaded_file_object.delete()
